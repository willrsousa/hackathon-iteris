package br.com.iteris.coins;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public interface CoinsService {

    @GET("ticker")
    Call<List<Coin>> getCoins();

    class Creator {
        public static CoinsService newService() {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://api.coinmarketcap.com/v1/ticker/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit.create(CoinsService.class);
        }
    }
}
