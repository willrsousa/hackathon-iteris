package br.com.iteris.coins;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {


    RecyclerView rvCoins;
    List<Coin> coins;
    CoinAdapter coinAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rvCoins = findViewById(R.id.rv_coins);
        coins = new ArrayList<>();
        coinAdapter = new CoinAdapter(this, coins);
        rvCoins.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvCoins.setAdapter(coinAdapter);
        loadCoins();
    }


    private void loadCoins() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.coinmarketcap.com/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        retrofit.create(CoinService.class)
                .getCoin()
                .enqueue(new Callback<List<Coin>>() {
                    @Override
                    public void onResponse(Call<List<Coin>> call, Response<List<Coin>> response) {
                        coins.addAll(response.body());
                        coinAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFailure(Call<List<Coin>> call, Throwable t) {
                        Log.e(getClass().getName(), t.getMessage());
                    }
                });
    }

}
