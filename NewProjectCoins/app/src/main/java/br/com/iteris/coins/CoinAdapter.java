package br.com.iteris.coins;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by rasc on 12/07/2018.
 */

public class CoinAdapter extends RecyclerView.Adapter<CoinAdapter.CoinHolder> {
    LayoutInflater layoutInflater;
    private List<Coin> coins;
    private Context context;

    public CoinAdapter(Context context, List<Coin> coins) {
        this.context = context;
        this.coins = coins;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public CoinHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.adapter_coin, parent, false);
        return new CoinHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CoinHolder holder, int position) {
        Coin coin = coins.get(position);
        Glide.with(context)
                .load("https://res.cloudinary.com/dxi90ksom/image/upload/"+coin.getSymbol()+".png")
                .into(holder.ivCoin);
        holder.tvSymbol.setText(coin.getSymbol());
        holder.tvName.setText(coin.getName());
        holder.tvPriceUsd.setText(coin.getPriceUsd());
    }

    @Override
    public int getItemCount() {
        return coins.size();
    }

    public static class CoinHolder extends RecyclerView.ViewHolder {

        ImageView ivCoin;
        TextView tvName;
        TextView tvSymbol;
        TextView tvPriceUsd;

        public CoinHolder(View itemView) {
            super(itemView);
            ivCoin = itemView.findViewById(R.id.iv_coin);
            tvName = itemView.findViewById(R.id.tv_name);
            tvSymbol = itemView.findViewById(R.id.tv_symbol);
            tvPriceUsd = itemView.findViewById(R.id.tv_price_usd);
        }
    }
}
