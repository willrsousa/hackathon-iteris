package br.com.iteris.coins;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.rv_coins)
    public RecyclerView rvCoins;

    CoinAdapter coinAdapter;
    List<Coin> coins;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        coins = new ArrayList<>();
        context = this;

        coinAdapter = new CoinAdapter(context, coins);
        rvCoins.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        rvCoins.setAdapter(coinAdapter);

        loadCoins();
    }

    private void loadCoins() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.coinmarketcap.com/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        retrofit.create(CoinsService.class)
                .getCoins()
                .enqueue(new Callback<List<Coin>>() {
                    @Override
                    public void onResponse(Call<List<Coin>> call, Response<List<Coin>> response) {
                        if (response.isSuccessful()) {
                            coins.clear();
                            coins.addAll(response.body());
                            coinAdapter.notifyDataSetChanged();
                        }
                        Log.d(getClass().getName(), response.toString());
                    }

                    @Override
                    public void onFailure(Call<List<Coin>> call, Throwable t) {
                        Log.e(getClass().getName(), t.getMessage());
                        Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }
}
